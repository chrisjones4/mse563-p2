"""
Takes in log file(s) and finds the window that equilibrated

"""
from sys import argv
import numpy as np
import matplotlib.pyplot as plt
import help_functions as hlp


list_of_dirs = list_of_logs = [vals for vals in argv[1:]]

for array_dir in list_of_dirs:

    raw_array = hlp.get_data(array_dir)
    PLOT = True
    BIN_SIZE = 50

    raw_x_values = raw_array[:, 0]
    raw_y_values = raw_array[:, 1]

    flipped_x_values = raw_x_values[::-1]
    flipped_y_values = raw_y_values[::-1]

    chunk_x_values = np.array_split(flipped_x_values,
                                    len(flipped_x_values)/BIN_SIZE)
    chunk_y_values = np.array_split(flipped_y_values,
                                    len(flipped_y_values)/BIN_SIZE)

    average_x_windows = [np.mean(i) for i in chunk_x_values]
    average_y_windows = [np.mean(i) for i in chunk_y_values]

    deviation_x_windows = [np.std(i) for i in chunk_x_values]
    deviation_y_windows = [np.std(i) for i in chunk_y_values]

    """
    Starting from the last frame we work our way to the first frame, checking
    to see if the current frame std is less than half of the next frame's std
    """
    for i, std_y in enumerate(deviation_y_windows):
        if i+1 < len(deviation_y_windows):
            if 1 > abs((deviation_y_windows[i+1]-std_y)/std_y):
                pass
            else:
                start_index = (len(deviation_y_windows)-(i))*BIN_SIZE
                break
        else:
            start_index = 0

    t = raw_x_values
    t = t-t[0]
    dt = t[1]-t[0]
    pe = raw_y_values
    t = t[start_index:]
    pe = pe[start_index:]

    ft = np.fft.rfft(pe-[np.average(pe) for i in range(len(pe))])
    acorr = np.fft.irfft(ft*np.conjugate(ft)) / (len(pe)*np.var(pe))
    acorr = [a for a in acorr[0:len(acorr)/2]]

    for acorr_i in range(len(acorr)):
        if acorr[acorr_i] < 0:
            break
    lags = [i*dt for i in range(len(acorr))]
    frameStart = t[0]
    frameStride = lags[acorr_i]
    nsamples = (t[-1]-frameStart) / frameStride

    
    

    # Create text file with $window.py > window.txt
    print("#start_frame num_samples auto_corr_time dt ave_pe ave_pe_std")
    print(float(start_index), str(nsamples), float(lags[acorr_i]), int(dt), np.mean(raw_y_values[start_index:]), np.std(raw_y_values[start_index:]))

    # Plot the functions if desired
    if PLOT is True:
        plt.errorbar(average_x_windows, average_y_windows, deviation_y_windows, linestyle='ro')  # Adds the points for the averages with the associated error bars
        plt.plot(raw_x_values, raw_y_values, 'ro')  # Plots the raw data under the error/averages.
        plt.plot(raw_x_values[start_index:], raw_y_values[start_index:], 'bo')
        plt.title(argv[1][:-4])
        plt.ylabel("PE")
        plt.xlabel("Time Step")
        plt.show()
 
    #Create text file with $window.py > window.txt
    #DANGER, assumes you passed it some/file/log
"""    
    with open("window.txt", "w") as text_file:
        text_file.write("#start_frame num_samples auto_corr_time dt ave_pe ave_pe_std")
        text_file.write(
"""
        #Plot the functions if desired
       #plt.savefig("window_plots/"+argv[1][:-4]+".pdf")
        # plt.savefig("window_plots/"+argv[1][:-4]+".pdf")
