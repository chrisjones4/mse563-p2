"""This module provides ways to read and write model XML files."""
import xml.etree.ElementTree as ET
from os.path import isfile 
from ast import literal_eval

class Atom():
    """Container class for atoms."""
    def __init__(self,a="?",b=0.0,c=1.,d=1.,i=0,m=1.,n="",t="A"):
        self.bond_type = a
        self.charge =b
        self.diameter=c
        self.epsilon=d
        self.id=i
        self.mass=m
        self.note=n
        self.type=t

class Bond():
    """Container class for bonds."""
    def __init__(self,k=30000.,l=1.5,ta="A",tb="A",ty="A-A"):
        self.k=k
        self.l=l
        self.typeA=ta
        self.typeB=tb
        self.type=ty

    def set_types(self):
        t = self.type.split('-')
        self.typeA=t[0]
        self.typeB=t[1]

class Angle():
    """Container class for angles."""
    def __init__(self,k=380.,t=1.95,ta="A",tb="A",tc="A",ty="A-A-A"):
        self.k=k
        self.theta=t
        self.typeA=ta
        self.typeB=tb
        self.typeC=tc
        self.type=ty

    def set_types(self):
        t = self.type.split('-')
        self.typeA=t[0]
        self.typeB=t[1]
        self.typeC=t[2]

class Dihedral():
    """Container class for dihedrals."""
    def __init__(self,k1=0.,k2=23.,k3=0.,k4=0.,ta="A",tb="A",tc="A",td="A",ty="A-A-A-A"):
        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        self.typeA=ta
        self.typeB=tb
        self.typeC=tc
        self.typeD=td
        self.type=ty

    def set_types(self):
        t = self.type.split('-')
        self.typeA=t[0]
        self.typeB=t[1]
        self.typeC=t[2]
        self.typeD=t[3]
        
class Improper():
    def __init__(self,k=10.,t=0.,ta="A",tb="A",tc="A",td="A",ty="A-A-A-A"):
        self.k=k
        self.theta=t
        self.typeA=ta
        self.typeB=tb
        self.typeC=tc
        self.typeD=td
        self.type=ty

    def set_types(self):
        t = self.type.split('-')
        self.typeA=t[0]
        self.typeB=t[1]
        self.typeC=t[2]
        self.typeD=t[3]

class ModelHandler():
    """Interface to MD_model XML files."""
    #TODO: Can we replace the ff class with this?
    def __init__(self,filename=None,name="default"):
        """Create an empty ModelHandler instance."""
        self.added_a_new_model = False
        self.mapping = {}
        self.atoms= {}
        self.bonds= {}
        self.angles= {}
        self.dihedrals= {}
        self.impropers= {}
        self.model_name = name
        if filename==None: #Create empty xml file
            self.filename="default_model.xml"
            print("No file specified, creating empty model file", self.filename)
            self.root = ET.Element("MD_model_xml",version="0.1")
            self.root.text='\n'
            self.new_model(model_name=self.model_name, note="initially empty model")
            self.write(self.filename)
        elif not isfile(filename):
            print("File not found, creating empty model file", filename)
            self.root = ET.Element("MD_model_xml",version="0.1")
            self.root.text='\n'
            self.new_model(model_name=self.model_name, note="initially empty model")
            self.write(filename)
            self.filename=filename
        else:
            self.filename=filename
            self.load_file(self.filename)
        self.enumerate_models()

    def write(self, filename):
        """Writes out XML file."""
        print("calling Write")
        if self.added_a_new_model: #TODO including this causes an error.
            self.root.insert(0,self.model)
            self.added_a_new_model = False
        self.enumerate_models()
        tree = ET.ElementTree(self.root)
        tree.write(filename, xml_declaration=True, encoding="UTF-8")

    def enumerate_models(self):
        """Counts how many instances of each model exist in a file."""
        model_names = []
        for c in self.root:
            model_names.append(c.attrib['name'])
        for i in list(set(model_names)):
            assert model_names.count(i) == 1
        print("\nONE COPY FOUND.\n")

    def load_file(self, filename):
        """Initializes the XML tree from an existing XML file."""
        print("Loading model file", filename)
        self.root = ET.parse(filename).getroot()
        for c in self.root:
            print(c.tag,c.attrib)
        print("done loading")

    def load_model(self,modelname):
        """Loads model from XML file if it exists, otherwise asks you to create a new one."""
        print("CALLING LOAD MODEL")
        self.model_name = modelname
        self.model = None
        for model in self.root:
            if model.get('name') == self.model_name:
                self.model = model
        if self.model==None:
            print("Couldn't find your model:", self.model_name)
            answer = input("Add it? (Y/N): ")
            if answer in ["Y", "y", "Yes", "YES", "yes"]:
                self.new_model(self.model_name,"New Model.")
            else: 
                exit()
        self.parse_mappings()
        self.parse_atoms()
        self.parse_bonds()
        self.parse_angles()
        self.parse_dihedrals()
        self.parse_impropers()
        print("Loaded", modelname)

    def __getitem__(self, key):
        return self.mapping[key]

    def new_model(self,model_name, note):
        print("I'M ADDING A NEW MODEL",model_name)
        self.model = ET.Element("model",name=model_name,note=note)
        self.model.text='\n'
        self.model.tail='\n'
        self.added_a_new_model = True
        
    def parse_atoms(self):
        self.atoms = {}
        for node in self.model.findall("atom"):
            bt = node.attrib['bond_type']
            c = float(node.attrib['charge'])
            d = float(node.attrib['diameter'])
            e = float(node.attrib['epsilon'])
            i = int(node.attrib['id'])
            m = float(node.attrib['mass'])
            n = node.attrib['note']
            t = node.attrib['type']
            self.atoms[t] = Atom(bt,c,d,e,i,m,n,t)

    def parse_bonds(self):
        self.bonds= {}
        for node in self.model.findall("bond"):
            k = float(node.attrib['k'])
            l = float(node.attrib['l'])
            ta = node.attrib['typeA']
            tb = node.attrib['typeB']
            ty = "{0}-{1}".format(ta,tb)
            self.bonds[ty] = Bond(k=k,l=l,ta=ta,tb=tb,ty=ty)

    def parse_angles(self):
        self.angles= {}
        for node in self.model.findall("angle"):
            k = float(node.attrib['k'])
            t = float(node.attrib['theta'])
            ta = node.attrib['typeA']
            tb = node.attrib['typeB']
            tc = node.attrib['typeC']
            ty = "{0}-{1}-{2}".format(ta,tb,tc)
            self.angles[ty] = Angle(k=k,t=t,ta=ta,tb=tb,tc=tc,ty=ty)

    def parse_dihedrals(self):
        self.dihedrals= {}
        for node in self.model.findall("dihedral"):
            k1 = float(node.attrib['k1'])
            k2 = float(node.attrib['k2'])
            k3 = float(node.attrib['k3'])
            k4 = float(node.attrib['k4'])
            ta = node.attrib['typeA']
            tb = node.attrib['typeB']
            tc = node.attrib['typeC']
            td = node.attrib['typeD']
            ty = "{0}-{1}-{2}-{3}".format(ta,tb,tc,td)
            self.dihedrals[ty] = Dihedral(k1=k1,k2=k2,k3=k3,k4=k4,ta=ta,tb=tb,tc=tc,td=td,ty=ty)

    def parse_impropers(self):
        self.impropers= {}
        for node in self.model.findall("improper"):
            k = float(node.attrib['k'])
            t = float(node.attrib['theta'])
            ta = node.attrib['typeA']
            tb = node.attrib['typeB']
            tc = node.attrib['typeC']
            td = node.attrib['typeD']
            ty = "{0}-{1}-{2}-{3}".format(ta,tb,tc,td)
            self.impropers[ty] = Improper(k=k,t=t,ta=ta,tb=tb,tc=tc,td=td,ty=ty)
            
    def parse_mappings(self):
        self.mapping= {}
        for node in self.model.findall("type_map"):
            bt = node.attrib['base']
            n = literal_eval(node.attrib['neighbors'])
            ring = literal_eval(node.attrib['ring'])
            self.mapping[(bt,n,ring)] = node.attrib['becomes']

    def normalize(self):
        self.load_model(self.model_name)
        mh = ModelHandler('temp.xml','normalized')
        mh.load_model('normalized')
        max_eps = 0.
        max_sig = 0.
        for k,v in self.atoms.items():
            if v.epsilon > max_eps:
                max_eps = v.epsilon
            if v.diameter> max_sig:
                max_sig = v.diameter
        for k,v in self.atoms.items():
            #v.diameter/=max_sig
            #v.epsilon/=max_eps
            mh.add_atom(v)
        for k,v in self.bonds.items():
            #v.k/=max_eps
            #v.k = 67.*v.k
            #v.k = 50.*v.k
            #v.l/=max_sig
            mh.add_bond(v)
        for k,v in self.angles.items():
            #v.k/=max_eps
            #v.k = 15627.*v.k
            v.k = v.k/(180./3.14159)**2.
            #v.theta=v.theta*3.14159/180.
            mh.add_angle(v)
        for k,v in self.dihedrals.items():
            #v.k1/=max_eps
            #v.k2/=max_eps
            #v.k3/=max_eps
            #v.k4/=max_eps
            mh.add_dihedral(v)
        mh.write('temp.xml')

    
    def add_atom(self,atom):
        m = ET.Element("atom", name=str(atom.id)
                             , bond_type=atom.bond_type
                             , charge=str(atom.charge)
                             , diameter=str(atom.diameter)
                             , epsilon=str(atom.epsilon)
                             , id=str(atom.id)
                             , mass=str(atom.mass)
                             , note=atom.note
                             , type=atom.type)

        m.tail='\n'
        self.model.append(m)
        self.atoms[atom.type] = atom

    def add_bond(self,bond):
        m = ET.Element("Bond", k=str(bond.k),l=str(bond.l),typeA=bond.typeA,typeB=bond.typeB)
        m.tail='\n'
        self.model.append(m)
        self.bonds[bond.type] = bond

    def add_angle(self,angle):
        m = ET.Element("angle", k=str(angle.k),theta=str(angle.theta),typeA=angle.typeA,typeB=angle.typeB,typeC=angle.typeC)
        m.tail='\n'
        self.model.append(m)
        self.angles[angle.type] = angle

    def add_dihedral(self,dihedral):
        m = ET.Element("dihedral", k1=str(dihedral.k1),k2=str(dihedral.k2),k3=str(dihedral.k3),k4=str(dihedral.k4),typeA=dihedral.typeA,typeB=dihedral.typeB,typeC=dihedral.typeC,typeD=dihedral.typeD)
        m.tail='\n'
        self.model.append(m)
        self.dihedrals[dihedral.type] = dihedral

    def add_improper(self,improper):
        m = ET.Element("improper", k=str(improper.k),theta=str(improper.theta),typeA=improper.typeA,typeB=improper.typeB,typeC=improper.typeC,typeD=improper.typeD)
        m.tail='\n'
        self.model.append(m)
        self.impropers[improper.type] = improper

    def add_mapping(self,mapping,becomes):
        m = ET.Element("type_map", base = mapping[0], neighbors=str(mapping[1]),ring=str(mapping[2]) , becomes=becomes)
        #m = ET.Element("type_map", base = mapping[0], neighbors=str(mapping[1]),ring=str(mapping[2]) , becomes=becomes)
        m.tail='\n'
        self.model.append(m)
        self.mapping[mapping]=becomes
        
    def get_atom(self,atom):
        if atom in list(self.atoms.keys()):
            return self.atoms[atom]
        else:
            print("Creating an empty atom in your file for", atom)
            a = Atom(t=atom)
            self.add_atom(a)

    def get_bond(self,bond):
        if bond in list(self.bonds.keys()):
            return self.bonds[bond]
        else:
            print("Creating an empty bond in your file for", bond)
            a = Bond(ty=bond)
            a.set_types()
            self.add_bond(a)

    def get_angle(self,angle):
        if angle in list(self.angles.keys()):
            return self.angles[angle]
        else:
            print("Creating an empty angle in your file for", angle)
            a = Angle(ty=angle)
            a.set_types()
            self.add_angle(a)

    def get_dihedral(self,dihedral):
        if dihedral in list(self.dihedrals.keys()):
            return self.dihedrals[dihedral]
        else:
            print("Creating empty dihedral in your file for", dihedral)
            a = Dihedral(ty=dihedral)
            a.set_types()
            self.add_dihedral(a)

    def get_improper(self,improper):
        if improper in list(self.impropers.keys()):
            return self.impropers[improper]
        else:
            print("Creating an empty improper in your file for", improper)
            a = Improper(ty=improper)
            a.set_types()
            self.add_improper(a)

    def get_mapping(self,mapping):
        if mapping in list(self.mapping.keys()):
            return self.mapping[mapping]
        else:
            b = input("Mapping "+str(mapping) +" not found.  Enter type to map this mapping to (e.g., 'C2'): ")
            self.add_mapping(mapping,b)
            return self.mapping[mapping]

    def set_atom(self,atom):
        for node in self.model.findall("atom"):
            if node.attrib['type']==str(atom.type):
                node.attrib['bond_type'] = str(atom.bond_type)
                node.attrib['charge'] = str(atom.charge)
                node.attrib['diameter'] = str(atom.diameter)
                node.attrib['epsilon'] = str(atom.epsilon)
                node.attrib['mass'] = str(atom.mass)
                return
        print("Couldn't find atom", atom.id)

    def set_bond(self,bond):
        for node in self.model.findall("bond"):
            if node.attrib['typeA']==bond.typeA and node.attrib['typeB']==bond.typeB:
                node.attrib['k'] = str(bond.k)
                node.attrib['l'] = str(bond.l)
                return
        print("Couldn't find bond", bond.typeA, bond.typeB)

    def set_angle(self,angle):
        for node in self.model.findall("angle"):
            if node.attrib['typeA']==angle.typeA and node.attrib['typeB']==angle.typeB and node.attrib['typeC']==angle.typeC:
                node.attrib['k'] = str(angle.k)
                node.attrib['theta'] = str(angle.theta)
                return
        print("Couldn't find angle", angle.typeA, angle.typeB, angle.typeC)

    def set_dihedral(self,dihedral):#only pass dihedrals with order suffix
        for node in self.model.findall("dihedral"):
            if node.attrib['typeA']==dihedral.typeA and node.attrib['typeB']==dihedral.typeB and node.attrib['typeC']==dihedral.typeC and node.attrib['typeD']==dihedral.typeD and node.attrib['order']==str(dihedral.order):
                node.attrib['V'] = str(dihedral.V)
                node.attrib['sign'] = str(dihedral.sign)
                return
        print("Couldn't find dihedral", dihedral.typeA, dihedral.typeB, dihedral.typeC, dihedral.typeD, dihedral.order)

    def set_dihedral_opls(self,dihedral):
        for node in self.model.findall("dihedral"):
            if node.attrib['typeA']==dihedral.typeA and node.attrib['typeB']==dihedral.typeB and node.attrib['typeC']==dihedral.typeC and node.attrib['typeD']==dihedral.typeD:
                node.attrib['k1'] = str(dihedral.k1)
                node.attrib['k2'] = str(dihedral.k2)
                node.attrib['k3'] = str(dihedral.k3)
                node.attrib['k4'] = str(dihedral.k4)
                return
        print("Couldn't find dihedral", dihedral.typeA, dihedral.typeB, dihedral.typeC, dihedral.typeD)

    def set_improper(self,improper): #TODO wrong
        for node in self.model.findall("improper"):
            if node.attrib['typeA']==improper.typeA and node.attrib['typeB']==improper.typeB and node.attrib['typeC']==improper.typeC and node.attrib['typeD']==improper.typeD:
                node.attrib['k'] = str(improper.k)
                node.attrib['theta'] = str(improper.theta)
                print("Found an improper!")
                return
        print("Couldn't find improper", improper.typeA, improper.typeB, improper.typeC, improper.typeD)

def load_opls_into_handler(model_file,model_name,op_file):
    """Uses an opls parameter file to populate sigmas, epsilons, charges, bonds, angles, dihedrals, 
       and impropers in a model within a model_file.  Note unit conversions from [kcal/mol] into [K].
    """
    mh = ModelHandler(model_file)
    mh.load_model(model_name)
    f = open(op_file,'r')
    atoms= {}
    type_map = {}
    group_map = {}
    vdw= {}
    charge = {}
    bonds= {}
    angles= {}
    dihedrals= {}
    impropers= {}
    #kc2K = 1.
    #d2r = 1.
    kc2K = 503.64
    d2r = 2*3.141592653589/360.

    #OPLS Specific stuff; the 503.64 converts kcal/mol -> Kelvin (1/kb)
    with open(op_file,'r') as f:
        for line in f:
            w = line.split()
            if len(w)>0:
                if w[0] == 'atom':
                    atoms[int(w[1])] = (int(w[2]),w[3],int(w[4]),float(w[5]),int(w[6]))
                if w[0] == 'vdw':
                    vdw[int(w[1])] = (float(w[2]),float(w[3])*kc2K)
                if w[0] == 'charge':
                    charge[int(w[1])] = float(w[2])
                if w[0] == 'bond':
                    bonds[(int(w[1]),int(w[2]))] = (float(w[3])*kc2K,float(w[4]))
                if w[0] == 'angle':
                    angles[(int(w[1]),int(w[2]),int(w[3]))] = (float(w[4])*kc2K,float(w[5])*d2r)
                if w[0] == 'torsion':
                    dihedrals[(int(w[1]),int(w[2]),int(w[3]),int(w[4]))] = (float(w[5])*kc2K,float(w[8])*kc2K,float(w[11])*kc2K)
                if w[0] == 'imptors':
                    impropers[(int(w[1]),int(w[2]),int(w[3]),int(w[4]))] = (float(w[5])*kc2K,float(w[6])*d2r)

    for node in mh.model.findall('atom'):
        i = int(node.attrib['id']) #Get id from model file and use this for everything. 
        if i == 0:
            print("Yo, you need to set the id's in your xml file or I can't look them up in opls-aa.")
            exit(1)
        type_map[node.attrib['type']] = i
        group_map[node.attrib['type']] = atoms[i][0]
        print(node.attrib['type'], group_map[node.attrib['type']])
        print(atoms[i][2],atoms[i][4],charge[i],vdw[i][0],vdw[i][1],i,atoms[i][3],node.attrib['type'])
        #a = Atom(a=atoms[i][2],b=atoms[i][4],c=charge[i],d=vdw[i][0],e=vdw[i][1],i=i,m=atoms[i][3],n="",t=node.attrib['type'])
        #mh.set_atom(a)
    not_found = []
    for node in mh.model.findall('bond'):
        k = [group_map[node.attrib['typeA']],group_map[node.attrib['typeB']]]
        k = tuple(sorted(k))
        if k not in list(bonds.keys()):
            print("Didn't find bond for", k, node.attrib['typeA'], node.attrib['typeB'])
            not_found.append(k)
            continue
        #a = Bond(k=bonds[k][0],l=bonds[k][1],ta=node.attrib['typeA'],tb=node.attrib['typeB'])
        a = Bond(k=3000.0,l=1.5,ta=node.attrib['typeA'],tb=node.attrib['typeB'])
        mh.set_bond(a)
    not_found = list(sorted(set(not_found)))
    print("Need bonds:")
    for n in not_found:
        print(n)
    not_found = []
    for node in mh.model.findall('angle'):
        k = (group_map[node.attrib['typeA']],group_map[node.attrib['typeB']],group_map[node.attrib['typeC']])
        if k[0]>k[-1]:
            k = k[::-1]
        if k not in list(angles.keys()):
            not_found.append(k)
            print("Didn't find angle for", k, node.attrib['typeA'], node.attrib['typeB'], node.attrib['typeC'])
            continue
        #a = Angle(k=angles[k][0],t=angles[k][1],ta=node.attrib['typeA'],tb=node.attrib['typeB'],tc=node.attrib['typeC'])
        a = Angle(k=380.0,t=1.95,ta=node.attrib['typeA'],tb=node.attrib['typeB'],tc=node.attrib['typeC'])
        mh.set_angle(a)
    not_found = list(sorted(set(not_found)))
    print("Need angles:")
    for n in not_found:
        print(n)
    not_found = []
    for node in mh.model.findall('dihedral'):
        k = (group_map[node.attrib['typeA']],group_map[node.attrib['typeB']],group_map[node.attrib['typeC']],group_map[node.attrib['typeD']])
        if k[0]>k[-1]:
            k = k[::-1]
        if k not in list(dihedrals.keys()):
            not_found.append(k)
            print("Didn't find dihedral for", k, node.attrib['typeA'], node.attrib['typeB'], node.attrib['typeC'], node.attrib['typeD'])
            continue
        #a = Dihedral(k1=node.attrib['k1'],k2=node.attrib['k2'],k3=node.attrib['k3'],k4=node.attrib['k4'],ta=node.attrib['typeA'],tb=node.attrib['typeB'],tc=node.attrib['typeC'],td=node.attrib['typeD'])
        a = Dihedral(k1=0.0,k2=23.0,k3=0.0,k4=0.0,ta=node.attrib['typeA'],tb=node.attrib['typeB'],tc=node.attrib['typeC'],td=node.attrib['typeD'])
        mh.set_dihedral_opls(a)
    not_found = list(sorted(set(not_found)))
    print("Need dihedrals:")
    for n in not_found:
        print(n)
    not_found = []
    for node in mh.model.findall('improper'):
        k = (group_map[node.attrib['typeA']],group_map[node.attrib['typeB']],group_map[node.attrib['typeC']],group_map[node.attrib['typeD']])
        if k[0]>k[-1]:
            k = k[::-1]
        if k not in list(impropers.keys()):
            not_found.append(k)
            print("Didn't find improper for", k, node.attrib['typeA'], node.attrib['typeB'], node.attrib['typeC'], node.attrib['typeD'])
            continue
        a = Improper(k=impropers[k][0],t=impropers[k][1],ta=node.attrib['typeA'],tb=node.attrib['typeB'],tc=node.attrib['typeC'],td=node.attrib['typeD'])
        mh.set_improper(a)
    not_found = list(sorted(set(not_found)))
    print("Need impropers:")
    for n in not_found:
        print(n)
    mh.write(mh.filename)
