"""Module for reading, writing, and building molecules from smaller molecules."""
import numpy
import itertools
from . import ff
from . import hoomd_xml
from .utilities import alphabetize
from .utilities import rotation_matrix_from_to
from .utilities import shift_indicies_in_list
from .utilities import remove_items_not_containing
from .utilities import remove_items_containing
from .model_handler import ModelHandler

class building_block:
    """Container class for molecules.

    Building blocks are the units from which larger molecules can be built and this class has
    the machinery for building larger blocks from smaller ones.  Building blocks also have machinery
    for taking bare-bones xml files and creating more 'full-featured' xml files that can be used in
    HOOMD simulations.

    Attributes:
        N: The number of atoms/particles comprising this building block>
        props: A dictionary of properties, with the keys initialized from the list 'property_names':
                position, mass, diameter, charge, type, body, bond, angle and dihedral are the usuals.
        props['position'] : Numpy array of N x,y,z coordinates.
        props['type'] : List of strings.
        props['mass'] : List of floats.
        props['diameter'] : List of floats.
        props['charge'] :  List of floats.
        props['body'] : List of ints.  -1 indicates not part of a rigid body.
        props['bond'] :  List of list of strings. (type a b)
        props['angle'] :  List of list of strings. (type a b c)
        props['dihedral'] :  List of list of strings. (type a b c d)
        props['improper'] :  List of list of strings. (type a b c d)
        props['molecule'] :  List of ints.
        props['residue'] :  List of ints.
        props['backbone'] :  List of ints.
        props['sidechain'] :  List of ints.
        neighbors: A list of neighbors corresponding to each particle.
    """

    def __init__(self,infile=None, model_file="default_model.xml",model_name="default"):
        """Creates empty lists for each name in property_names and calls from_xml() if there is an infile."""
        self.property_names = hoomd_xml.main_particle_properties +hoomd_xml.constraints
        self.props={}
        for p in self.property_names:
            self.props[p] = []
        self.N=0
        if infile is not None:
            self.from_xml(infile)
        self.print_constraints = False
        self.model_name = model_name
        self.model_file = model_file

    def set_defaults(self):
        """Gives masses, diameters, charge, body, residue, backbone and sidechain values sane defaults."""
        self.get_N_atoms()
        for p in self.property_names:
            if len(self.props[p]) == 0:
                for i in range(self.N):
                    if p == 'type':
                        self.props[p].append('A')
                    if p in ['mass', 'diameter']:
                        self.props[p].append(1.0)
                    if p == 'charge':
                        self.props[p].append(0.0)
                    if p in ['body','molecule','residue','backbone','sidechain']:
                        self.props[p].append(-1)

    def setup_model_handler(self):
        """Creates a ModelHandler from model_file and model_name."""
        self.mh = ModelHandler(self.model_file,self.model_name)
        self.mh.load_model(self.model_name)

    def from_xml(self,filename):
        """Parses a hoomd_xml (filename) into a building block.
            Currently only positions, types, mass, diameter, charge, bond, angle dihedral, and impropers are set. """
        xml = hoomd_xml.hoomd_xml()
        xml.read(filename)

        xyz =  xml.config.find('position').text.split()
        self.N = int(len(xyz)/3 )
        self.props['position'] = numpy.zeros(shape=(self.N,3))
        for i in range(self.N):
            self.props['position'][i] = numpy.array([float(x) for x in xyz[i*3:i*3+3]])

        for child in xml.config:
            if child.tag=='type':
                self.props['type'] = xml.config.find('type').text.split()
            if child.tag in ['body','molecule','residue','backbone','sidechain', 'image']:
                self.props[child.tag] = [int(i) for i in xml.config.find(child.tag).text.split()]
            if child.tag in ['mass','diameter','charge']:
                self.props[child.tag] = [float(i) for i in xml.config.find(child.tag).text.split()]
            if child.tag=='bond':
                bonds = xml.config.find('bond').text.split()
                self.props['bond'] = [bonds[i:i+3] for i in range(0,len(bonds),3) ]
            if child.tag=='angle':
                angles = xml.config.find('angle').text.split()
                self.props['angle'] = [angles[i:i+4] for i in range(0,len(angles),4) ]
            if child.tag=='dihedral':
                dihedrals = xml.config.find('dihedral').text.split()
                self.props['dihedral'] = [dihedrals[i:i+5] for i in range(0,len(dihedrals),5) ]
            if child.tag=='improper':
                impropers = xml.config.find('improper').text.split()
                self.props['improper'] = [impropers[i:i+5] for i in range(0,len(impropers),5) ]
        #XML needs to be closed?  TODO

    def write(self, fname="test.xml", prop_list=['box','position']+hoomd_xml.main_particle_properties+hoomd_xml.constraints):
        """Writes out a building block as a hoomd xml file.

        Args:
            fname (str): Name of xml file to be written.
            prop_list (list): Optional list of properties to be written.
        """
        self.get_N_atoms()
        #self.center()
        xml = hoomd_xml.hoomd_xml()
        xml.from_building_block(self)
        xml.write(fname,prop_list)

    def center(self):
        """Translates geometrical center to origin."""
        com = numpy.array([0.,0.,0.])
        for p in self.props['position']:
            com += numpy.array(p)
        com /= float(self.get_N_atoms())
        self.translate(-com)

    def translate(self,d):
        """Translates all particles by vector 'd'."""
        for i,p in enumerate(self.props['position']):
            self.props['position'][i] = p+d

    def rotate(self,R):
        """Rotates all particles by rotation matrix R."""
        self.props['position'] = numpy.dot(self.props['position'],numpy.transpose(R))

    def scale(self, factor=1.0):
        """Scales all of the particle positions relative to the origin."""
        self.center()
        for i,p in enumerate(self.props['position']):
            self.props['position'][i] = factor*p

    def get_N_atoms(self):
        """Updates N and returns it."""
        self.N = len(self.props['position'])
        return self.N

    def populate_neighborlist(self):
        """Generates the neighborlist, requires bondlist in props['bond']."""
        links = sorted([(int(b[1]),int(b[2])) for b in self.props['bond']])
        self.neighbors = [ [] for i in self.props['position']]
        for l in links:
            self.neighbors[l[0]].append(l[1])
            self.neighbors[l[1]].append(l[0])

    def remove_redundant_rigid_constraints(self):
        """Finds angles, dihedrals, and impropers that are all within the same rigid body, and removes them."""
        angles= []
        dihedrals=[]
        impropers=[]
        for b in self.props['angle']:
            bodies = [self.props['body'][int(b[i])] for i in range(1,4)]
            if bodies[0] == bodies[1] and bodies[1]==bodies[2] and int(bodies[0])!=-1:
                pass
            else:
                angles.append(b)
        for b in self.props['dihedral']:
            bodies = [self.props['body'][int(b[i])] for i in range(1,5)]
            if bodies[0] == bodies[1] and bodies[1]==bodies[2] and bodies[2]==bodies[3] and int(bodies[0])!=-1:
                pass
            else:
                dihedrals.append(b)
        for b in self.props['improper']:
            bodies = [self.props['body'][int(b[i])] for i in range(1,5)]
            if bodies[0] == bodies[1] and bodies[1]==bodies[2] and bodies[2]==bodies[3] and int(bodies[0])!=-1:
                pass
            else:
                impropers.append(b)
        self.props['angle']=angles
        self.props['dihedral']=dihedrals
        self.props['improper']=impropers

    def neutralize_charge(self):
        N = self.get_N_atoms()
        total = sum(self.props['charge'])
        positive = sum([i for i in self.props['charge'] if i >0.])
        np = sum([1 for i in self.props['charge'] if i >0.])
        negative = sum([i for i in self.props['charge'] if i <0.])
        nn = sum([1 for i in self.props['charge'] if i <0.])
        ratio = np/float(nn)
        #for i,c in enumerate(self.props['charge']):
        #    if c<0.:
        #        self.props['charge'][i] = -1.*ratio
        #    if c>0.:
        #        self.props['charge'][i] = 1.

        if total < 0.:
            scale = positive/negative
            for i,c in enumerate(self.props['charge']):
                if c<0.:
                    self.props['charge'][i] = -c*scale
        else:
            print("HI",positive,negative)
            scale = negative/positive
            print(scale)
            for i,c in enumerate(self.props['charge']):
                if c>0.:
                    self.props['charge'][i] = -c*scale

        total = sum(self.props['charge'])
        print(total)

    def set_force_field_specific_parameters(self):
        """Sets building block parameters.

        This function uses the ff module and an input file to determine the LJ parameters,
        charges, masses, diameters, and dihedral types.  Needs types for a particular force field.
        Scales down building block to appropriate size.

        Args:
            infile (str): Name of force-field xml file to use for mappings.
        """
        my_ff = ff.hoomd_ff(self.model_file,self.model_name)
        my_ff.generate_LJ(list(set(self.props['type'])))
        my_ff.generate_charges(self.props['type'])
        my_ff.generate_masses(self.props['type'])
        my_ff.generate_diameters(self.props['type'])
        dihedral_types = list(set( [i[0] for i in self.props['dihedral']] ))

        d_temp = []
        self.props['charge']=my_ff.charges
        self.props['mass']=my_ff.masses
        self.props['diameter']=my_ff.diameters
        p1 = int(self.props['bond'][0][1])
        p2 = int(self.props['bond'][0][2])
        d = numpy.linalg.norm(self.props['position'][p2]-self.props['position'][p1])
        bond_length = my_ff.get_bond_length(self.props['bond'][0][0])
        self.scale(bond_length/d)
        self.remove_redundant_rigid_constraints()
        #TODO: add impropers

    def get_parameters_from_model(self):
        """Sets building block parameters.

        This function uses the ff module and an input file to read in mass, diameter, and
        eplsilon verbatim.
        Args:
            infile (str): Name of force-field xml file to use for mappings.
        """
        my_ff = ff.hoomd_ff(self.model_file,self.model_name)
        m,s,e = my_ff.read_atom_params(self.props['type'])
        self.props['mass']=m
        self.props['diameter']=s
        self.props['epsilon']=e
        p1 = int(self.props['bond'][0][1])
        p2 = int(self.props['bond'][0][2])
        d = numpy.linalg.norm(self.props['position'][p2]-self.props['position'][p1])
        b = self.props['bond'][0][0].split('-')
        bond_length = my_ff.db.bond(b[0],b[1])[0]
        print(bond_length)
        self.scale(bond_length/d)
        self.remove_redundant_rigid_constraints()

    def recast_bonds(self):
        """Resets each type of bond from the types of associated atoms and the bond order."""
        bond_types = []
        bond_order = []
        for b in self.props['bond']:
            if b[0]=='vis': #'vis' bonds are a trick for drawing bonds in rigid bodies 
                bond_types.append('siv')
                bond_order.append('')
            else:
                bond_types.append((self.props['type'][int(b[1])],self.props['type'][int(b[2])]))
                #bond_order.append(b[0][-1])
                bond_order.append('')
        bond_types = alphabetize(bond_types)
        bond = []
        for a,c in zip(bond_types,self.props['bond']):
            if a=='siv':
                pass
                bond.append(['vis',c[1],c[2]])
            else:
                bond.append([self.constraint_to_string(a),c[1],c[2]])
        bond_types=[self.constraint_to_string(i) for i in bond_types]
        self.props['bond']=bond
        if self.print_constraints==True:
            print("Bonds:")
            for i in list(set(bond_types)):
                print(i)
        for t in self.props['bond']:
            if t[0]!='vis':
                a = self.mh.get_bond(t[0])

    def tuple_to_types(self,tup):
        """Maps indices from a tuple to their atom types and returns a list of types."""
        s=[]
        for t in tup:
            s.append(self.props['type'][t])
        return tuple(s)

    def constraint_to_string(self, const):
        """Helper function turns an angle, bond, dihedral, or improper tuple to a string."""
        #TODO: This is a job for __repr__
        s =""
        for c in const[:-1]:
            s+=c+"-"
        s+=const[-1]
        return s

    def determine_types(self):
        """Based upon the bonds of each atom, determine its type."""
        #TODO: Want to have some sort of distinction between "basic" particle types and final types?
        self.setup_model_handler()
        list_of_ntypes = []
        order = []
        self.populate_neighborlist()
        self.find_rings()
        maxO=0
        for n in self.neighbors:
            if len(n)>maxO:
                maxO=len(n)
        print("MaxO=",maxO)
        for o in range(maxO+1):
            print("working on", o)
            order.append([ (i,n,self.ring[i]) for i,n in enumerate(self.neighbors) if len(n) == o])
            order[-1] = sorted(order[-1],key = lambda tup : min([len([n]) for n in self.neighbors[tup[0]]]) )
            for p in order[-1]:
                print("particle", p[0])
                neighbor_types = []
                base_type = self.props['type'][p[0]]
                for n in p[1]:
                    neighbor_types.append(self.props['type'][n])
                list_of_ntypes.append((base_type,tuple(sorted(neighbor_types)),p[2]))
                b = self.mh.get_mapping(list_of_ntypes[-1])
                self.props['type'][p[0]] = b
                print("mapped to", b)
            types = sorted(list(set(list_of_ntypes)))
            print(o, len(types))
            for i in types: 
                print(i)
        #Check that each of our atom types is in our model
        for i,t in enumerate(self.props['type']):
            #print i,t
            a = self.mh.get_atom(t)

    def find_angles(self):
        """Finds unique angle constraints and their types."""
        angles = []
        for i in range(self.get_N_atoms()):
            for n1 in self.neighbors[i]:
                for n2 in self.neighbors[n1]:
                    if n2 !=i:
                        if n2>i:
                            angles.append( (i,n1,n2))
                        else:
                            angles.append( (n2,n1,i))
        angles = sorted(set(alphabetize(angles)))
        angle_types = []
        for t in angles:
            angle_types.append(self.tuple_to_types(t))
        angle_types=alphabetize(angle_types)
        angle_types = [self.constraint_to_string(i) for i in angle_types]
        d = []
        for a,b in zip(angle_types,angles):
            d.append([a,b[0],b[1],b[2]])
        self.props['angle'] = d
        if self.print_constraints==True:
            print("Angles:")
            for i in list(set(angle_types)):
                print(i)
        #Check that each of our angle types is in our model
        for t in self.props['angle']:
            a = self.mh.get_angle(t[0])
        
    def find_dihedrals(self):
        """Finds unique dihedral constraints and impropers and their types."""
        dihedrals = []
        for i in range(self.get_N_atoms()):
            for n1 in self.neighbors[i]:
                for n2 in self.neighbors[n1]:
                    if n2 != i:
                        for n3 in self.neighbors[n2]:
                            if (n3 !=i) and (n3 !=n1):
                                if n3>i:
                                    dihedrals.append( (i,n1,n2,n3))
                                else:
                                    dihedrals.append( (n3,n2,n1,i))
        dihedrals = sorted(set(alphabetize(dihedrals)))
        dihedral_types = []
        for t in dihedrals:
            dihedral_types.append(self.tuple_to_types(t))
        dihedral_types=alphabetize(dihedral_types)
        dihedral_types = [self.constraint_to_string(i) for i in dihedral_types]
        d = []
        im = []
        for a,b in zip(dihedral_types,dihedrals):
            im.append([a,b[0],b[1],b[2],b[3]])
            d.append([a,b[0],b[1],b[2],b[3]])
        self.props['dihedral'] = d
        self.props['improper'] = im
        if self.print_constraints==True:
            print("Dihedrals:")
            for i in list(set(dihedral_types)):
                print(i)
        #Check that each of our angle and dihedral types is in our model
        for t in self.props['dihedral']:
            a = self.mh.get_dihedral(t[0])
        for t in self.props['improper']:
            a = self.mh.get_improper(t[0])

    def generate_all_constraints(self,print_constraints=False):
        """Uses the bonds to generate a neighborlist, which is used to create lists of all the angles and dihedrals."""
        self.print_constraints=False
        if print_constraints==True:
            self.print_constraints=True
        self.determine_types()
        self.recast_bonds()
        self.find_angles()
        self.find_dihedrals()
        self.mh.write(self.model_file)

    def find_atom_types(self):
        """Determines nearest neighborhood for each atom."""
        self.populate_neighborlist()
        self.find_rings()
        self.neighborhood = []
        for t,r,n in zip(self.props['type'],self.ring,self.neighbors):
            self.neighborhood.append((t,int(r),tuple(sorted([self.props['type'][int(i)] for i in n]))))

    def refine_rings(self):
        """Walks the shortest ring for each particle in a ring and sets leftovers as not in ring"""
        #TODO
        #ring_id = [0 if i True else 0 ]
        return


    def find_rings(self):
        """Determines if each atom is in a ring"""
        #TODO: Won't work for a bridge of atoms between two rings O----O
        ring_possible = [True]*self.get_N_atoms()
        for i,n in enumerate(self.neighbors):
            if len(n)<=1:
                ring_possible[i]=False
        flag = True
        while(flag):
            flag=False
            for i,n in enumerate(self.neighbors):
                if ring_possible[i]:
                    a = [ring_possible[j] for j in n]
                    if a.count(True)<=1:
                        ring_possible[i]=False
                        flag=True
        self.ring=ring_possible

    def shift_index_to_origin(self,i):
        """Translates all particles so particle i is at the origin"""
        self.translate(-self.props['position'][i])

    def increment_body(self,n=0):
        """Increases by n+1 the body value of all particles with non-(-1) body values."""
        if 'body' in list(self.props.keys()):
            for i,b in enumerate(self.props['body']):
                if b!=-1:
                    self.props['body'][i]+=n+1

    def remove_atom(self,i):
        """ Removes atom i from this building block.

        Removing an atom/particle requires removing all associated bonds, angles, and dihedrals and updating 
        all of the indices in these constraints.

        Args:
            i (int): index of atom or particle to remove

        Returns:
            bondsite (int): The index of the atom the removed atom was bonded to
            bondvec (numpy.ndarray): The bond vector that used to connect the removed particle to its bondsite
        """
        for b in self.props['bond']: 
            if b[1]==str(i):
                bondsite=b[2]
            if b[2]==str(i):
                bondsite=b[1]
        bondvec = self.props['position'][i] - self.props['position'][int(bondsite)] 
        self.props['position'] = numpy.vstack([self.props['position'][:i],self.props['position'][i+1:]])
        for a in hoomd_xml.main_particle_properties:
            self.props[a] = self.props[a][:i]+self.props[a][i+1:]
        for a in hoomd_xml.constraints:
            self.props[a] = shift_indicies_in_list(remove_items_containing(self.props[a],str(i)),i+1,-1)
        if i>int(bondsite):
            return int(bondsite), bondvec
        return int(bondsite)-1, bondvec

    def fuse_bond(self,my_site, bb_to_add, add_site):
        """Fuse two building blocks by removing two atoms and fusing their dangling bonds.

        Args:
            my_site (int): Index of atom to be removed on this building block.
            bb_to_add (building_block): Building block to be bonded to this one.
            add_site (int): Index of atom to be removed from the building block to add.

        Returns:
            None
        """
        my_site_id, my_vec = self.remove_atom(my_site)
        add_site_id, add_vec  = bb_to_add.remove_atom(add_site)
        my_vec /= numpy.linalg.norm(my_vec)/1.5 #The 2 is to make the bond a little longer
        add_vec /= numpy.linalg.norm(add_vec)
        R = rotation_matrix_from_to(add_vec,-my_vec)
        bb_to_add.rotate(R)
        bb_to_add.shift_index_to_origin(add_site_id)
        add_site_id += self.get_N_atoms()
        bb_to_add.translate(my_vec)
        self.shift_index_to_origin(my_site_id)
        for a in hoomd_xml.constraints:
            bb_to_add.props[a] = shift_indicies_in_list(bb_to_add.props[a],0,self.get_N_atoms())
        if 0 in self.props['body']:
            bb_to_add.increment_body(max(self.props['body']))
        self.props['position'] = numpy.vstack([self.props['position'] , bb_to_add.props['position']])
        for a in hoomd_xml.main_particle_properties+hoomd_xml.constraints:
            self.props[a] = self.props[a]+bb_to_add.props[a]
            print("Now we have", len(self.props[a]), a)
        self.props['bond'] += [['B1', str(my_site_id), str(add_site_id)]]
        self.generate_all_constraints()

    def join(self,i_aa,i_ab, bb_to_add, j_ba,j_bb):
        """Join two building blocks by creating a bond between two of their particles.

        i_ab and j_bb are used to deterine the bonding vectors 

        Args:
            i_aa (int): Index of particle on this building block to be bonded.
            i_ab (int): Index of nearest neighbor on this building block.
            bb_to_add (building_block): The building block to bond to this one.
            j_ba (int): Index of particle on bb_to_add to bond to this one.
            j_bb (int): Index of nearest neighbor to j_ba.

        Returns:
            None
        """
        my_vec = self.props['position'][i_aa]-self.props['position'][i_ab]
        my_vec /= numpy.linalg.norm(my_vec)/2.0 #The 2 is to make the bond a little longer
        add_vec = bb_to_add.props['position'][j_bb]-bb_to_add.props['position'][j_ba]
        add_vec /= numpy.linalg.norm(add_vec)
        R = rotation_matrix_from_to(add_vec,-my_vec)
        bb_to_add.rotate(R)
        bb_to_add.shift_index_to_origin(j_ba)
        j_ba += self.get_N_atoms()
        bb_to_add.translate(my_vec)
        self.shift_index_to_origin(i_aa)
        for a in ['bond','angle','dihedral']:
            bb_to_add.props[a] = shift_indicies_in_list(bb_to_add.props[a],0,self.get_N_atoms())
        self.props['position'] = numpy.vstack([self.props['position'] , bb_to_add.props['position']])
        if 0 in self.props['body']:
            bb_to_add.increment_body(max(self.props['body']))
        for a in ['mass','diameter','charge','type','body','bond','angle','dihedral']:
            self.props[a] = self.props[a]+bb_to_add.props[a]
        self.generate_all_constraints()

    def join_along(self,my_site,my_vec, bb_to_add, add_site,add_vec, make_bond = True):
        """Join two building blocks by creating a bond between two of their particles.

        i_ab and j_bb are used to deterine the bonding vectors 

        Args:
            my_site (int): Index of particle on this building block to be bonded.
            my_vec (numpy.ndarray): Vector which new bond will sit along
            bb_to_add (building_block): The building block to bond to this one.
            add_site (int): Index of particle on bb_to_add to bond to this one.
            add_vec (numpy.ndarray): Vector from add_site that will point along -my_vec
            make_bond: if set to False bonds will not form
        Returns:
            None
        """
        R = rotation_matrix_from_to(add_vec,-my_vec)
        if my_site >= self.get_N_atoms():
            print("Yo, that makes no sense.")
            exit()
        if add_site >= bb_to_add.get_N_atoms():
            print("Yo, that makes no sense.")
            exit()
        bb_to_add.rotate(R)
        bb_to_add.shift_index_to_origin(add_site)
        add_site += self.get_N_atoms()
        bb_to_add.translate(my_vec)
        self.shift_index_to_origin(my_site)
        for a in hoomd_xml.constraints:
            bb_to_add.props[a] = shift_indicies_in_list(bb_to_add.props[a],0,self.get_N_atoms())
        self.props['position'] = numpy.vstack([self.props['position'] , bb_to_add.props['position']])
        for a in hoomd_xml.main_particle_properties+hoomd_xml.constraints:
            self.props[a] = self.props[a]+bb_to_add.props[a]
        if make_bond == True:
            self.props['bond'] += [['B1', str(my_site), str(add_site)]]
            self.generate_all_constraints()

    def join_rigid(self,i_aa,i_ab, bb_to_add, j_ba,j_bb):
        """Join two building blocks by creating a bond between two of their particles.

        i_ab and j_bb are used to deterine the bonding vectors.
        This removes unneeded angles and dihedrals.  Be careful with 'vis' bonds.

        Args:
            i_aa (int): Index of particle on this building block to be bonded.
            i_ab (int): Index of nearest neighbor on this building block.
            bb_to_add (building_block): The building block to bond to this one.
            j_ba (int): Index of particle on bb_to_add to bond to this one.
            j_bb (int): Index of nearest neighbor to j_ba.

        Returns:
            None
        """
        my_vec = self.props['position'][i_aa]-self.props['position'][i_ab]
        my_vec /= numpy.linalg.norm(my_vec)/4.0 #the 2 is to make the bond a little longer
        add_vec = bb_to_add.props['position'][j_bb]-bb_to_add.props['position'][j_ba]
        add_vec /= numpy.linalg.norm(add_vec)
        R = rotation_matrix_from_to(add_vec,-my_vec)
        bb_to_add.rotate(R)
        bb_to_add.shift_index_to_origin(j_ba)
        j_ba += self.get_N_atoms()
        bb_to_add.translate(my_vec)
        self.shift_index_to_origin(i_aa)
        for a in hoomd_xml.constraints:
            bb_to_add.props[a] = shift_indicies_in_list(bb_to_add.props[a],0,self.get_N_atoms())
        self.props['position'] = numpy.vstack([self.props['position'] , bb_to_add.props['position']])
        if 0 in self.props['body']:
            bb_to_add.increment_body(max(self.props['body']))
        for a in hoomd_xml.main_particle_properties+hoomd_xml.constraints:
            self.props[a] = self.props[a]+bb_to_add.props[a]
        self.generate_all_constraints()

    def join_along_rigid(self,my_site,my_vec, bb_to_add, add_site,add_vec):
        """Join two building blocks by creating a bond between two of their particles.

        i_ab and j_bb are used to deterine the bonding vectors 

        Args:
            my_site (int): Index of particle on this building block to be bonded.
            my_vec (numpy.ndarray): Vector which new bond will sit along
            bb_to_add (building_block): The building block to bond to this one.
            add_site (int): Index of particle on bb_to_add to bond to this one.
            add_vec (numpy.ndarray): Vector from add_site that will point along -my_vec

        Returns:
            None
        """
        self.center()
        bb_to_add.center()
        R = rotation_matrix_from_to(add_vec,-my_vec)
        if my_site >= self.get_N_atoms():
            print("Yo, that makes no sense.")
            exit()
        if add_site >= bb_to_add.get_N_atoms():
            print("Yo, that makes no sense.")
            exit()

        bb_to_add.rotate(R)
        bb_to_add.shift_index_to_origin(add_site)
        add_site += self.get_N_atoms()
        bb_to_add.translate(my_vec)
        self.shift_index_to_origin(my_site)
        for a in hoomd_xml.constraints:
            bb_to_add.props[a] = shift_indicies_in_list(bb_to_add.props[a],0,self.get_N_atoms())
        if 0 in self.props['body']:
            bb_to_add.increment_body(max(self.props['body']))
        self.props['position'] = numpy.vstack([self.props['position'] , bb_to_add.props['position']])
        for a in hoomd_xml.main_particle_properties+hoomd_xml.constraints:
            self.props[a] = self.props[a]+bb_to_add.props[a]
            print("Now we have", len(self.props[a]), a)
        self.props['bond'] += [['B1', str(my_site), str(add_site)]]
        self.generate_all_constraints()
    
def cml2xml(infile,outfile,model_file,model_name):
    """Converts a cml file from Avogadro into a hoomd_xml file.

    Args:
        infile (str): Name of cml file to convert.
        outfile (str): name of xml file to be written.
        model_file (str): Name of model file to look for model name in.
        model_name (str): name of model to look for in model file.

    Returns:
        None
    """
    a = hoomd_xml.hoomd_xml(infile)
    a.write(outfile)
    molA = building_block(outfile, model_name=model_name, model_file=model_file)
    molA.generate_all_constraints()
    molA.write(outfile)
    
