import xml.etree.ElementTree as ET
import numpy
import os

class interaction_database():
    """ This class provides machinery for opening model xml files, and for reading properties from these files into building blocks."""
    def __init__(self,filename="default_model.xml", model_name="default", tabulated_dihedral=False):
        """Open model file and load model_name."""
        self.tabulated_dihedral = tabulated_dihedral
        self.root = ET.parse(filename).getroot()  #each element of dbase is a atom, bond, angle, or dihedral
        for model in self.root:
            if model.get('name') == model_name:
                self.dbase = model

    def get_ids_of_type(self, t):
        """Returns 'id' field for each type in the list 't'."""
        ids = []
        for atom in self.dbase.iter("atom"):
            if atom.get('type')==t:
                ids.append(int(atom.get('id')))
        return ids

    def sigma(self, i, key='id'):
        """Looks for an atom element in the database whose 'key' matches 'i' and returns its diameter."""
        for atom in self.dbase.iter("atom"):
            if atom.get(key)==i:
                return float(atom.get('diameter'))
        raise LookupError("Atom " +str(i)+" not found")
        exit()


    def charge(self, i,key='id'):
        """Looks for an atom element in the database whose 'key' matches 'i' and returns its charge."""
        for atom in self.dbase.iter("atom"):
            if atom.get(key)==i:
                return float(atom.get('charge'))
        raise LookupError("Atom " +str(i)+" not found")
        exit()

    def epsilon(self,i,key='id'):
        """Looks for an atom element in the database whose 'key' matches 'i' and returns its epsilon."""
        for atom in self.dbase.iter("atom"):
            if atom.get(key)==i:
                return float(atom.get('epsilon'))
        raise LookupError("Atom " +str(i)+" not found")
        exit()

    def mass(self,i,key='id'):
        """Looks for an atom element in the database whose 'key' matches 'i' and returns its mass."""
        for atom in self.dbase.iter("atom"):
            if atom.get(key)==i:
                return float(atom.get('mass'))
        raise LookupError("Atom " +str(i)+" not found")
        exit()

    def atype(self,i):
        """Looks for atom element in the database whose 'id' attribute  matches 'i' and returns its type."""
        for atom in self.dbase.iter("atom"):
            if int(atom.get('id'))==i:
                return atom.get('type')
        raise LookupError("atom " +str(i)+" not found")
        exit()

    def bond(self, a,b):
        """Looks for a bond element based on the types of atoms on either end.

            Returns a tuple of (bond length, bond k)
        """
        for bond in self.dbase.iter("bond"):
            if bond.get('typeA')==a and bond.get('typeB')==b:
                return (float(bond.get('l')), float(bond.get('k')))
        raise LookupError("bond " +a+" "+b+ " not found")
        exit()

    def angle(self, a,b,c):
        """Looks for an angle element based on the types of atoms in the constraint.

            Returns a tuple of (equilibrium angle, bending k)
        """
        for angle in self.dbase.iter("angle"):
            if angle.get('typeA')==a and angle.get('typeB')==b and angle.get('typeC')==c:
                return (float(angle.get('theta')), float(angle.get('k')))
        raise LookupError("angle " +str(a)+" "+str(b)+" "+str(c)+ " not found")
        exit()

    def dihedral(self, a,b, c,d):
        """Looks for an dihedral element based on the types of atoms in the constraint.

            Returns a tuple of (k1,k2,k3,k4)
        """
        for dihedral in self.dbase.iter("dihedral"):
            if dihedral.get('typeA')==a and dihedral.get('typeB')==b and dihedral.get('typeC')==c and dihedral.get('typeD')==d :
                return (float(dihedral.get('k1')) , float(dihedral.get('k2')),float(dihedral.get('k3')),float(dihedral.get('k4')))
        raise LookupError("dihedral " +str(a)+" "+str(b)+" "+str(c)+" "+str(d)+ " not found")
        if self.tabulated_dihedral:
            for dihedral in self.dbase.iter("dihedral"):
                if dihedral.get('typeA')==a and dihedral.get('typeB')==b and dihedral.get('typeC')==c and dihedral.get('typeD')==d :
                    return (float(dihedral.get('k0'))
                            , float(dihedral.get('k1'))
                            , float(dihedral.get('k2'))
                            , float(dihedral.get('k3'))
                            , float(dihedral.get('k4')))
            raise LookupError("dihedral " +str(a)+" "+str(b)+" "+str(c)+" "+str(d)+ " not found")
        else:
            for dihedral in self.dbase.iter("dihedral"):
                if dihedral.get('typeA')==a and dihedral.get('typeB')==b and dihedral.get('typeC')==c and dihedral.get('typeD')==d :
                    return (float(dihedral.get('k1')) , float(dihedral.get('k2')),float(dihedral.get('k3')),float(dihedral.get('k4')))
            raise LookupError("dihedral " +str(a)+" "+str(b)+" "+str(c)+" "+str(d)+ " not found")
        exit()

def opls_e_to_J(a):
    """OPLS kcal/mol -> J"""
    av = 6.02e23 #/mol
    return a*4184./av

def trappe_e_to_J(a):
    """TraPPE K -> J"""
    kb = 1.38e-23 #J/K
    return a*kb

class hoomd_ff():
    """Class for generating hoomd force fields."""
    def __init__(self, infile="default_model.xml", model_name="default", tabulated_dihedral=False):
        self.infile = infile
        self.tabulated_dihedral = tabulated_dihedral
        self.db = interaction_database(self.infile,model_name)

    def read_atom_params(self,types):
        masses = []
        sigmas=  []
        epsilons =  []
        for t in types:
            masses.append(self.db.mass(t,key='type'))
            sigmas.append(self.db.sigma(t,key='type'))
            epsilons.append(self.db.epsilon(t,key='type'))
        return masses, sigmas, epsilons

    def read_constraint_params(self,bonds,angles,dihedrals):
        b= {}
        a= {}
        d= {}
        for t in bonds:
            i = t.split('-')
            bx = self.db.bond(i[0],i[1])
            b[t] = (bx[0], bx[1])
        for j in angles:
            i = j.split('-')
            ax = self.db.angle(i[0],i[1], i[2])
            a[j] = (ax[0], ax[1])
        if self.tabulated_dihedral:
            for j in dihedrals:
                print(j)
                i = j.split('-')
                dx = self.db.dihedral(i[0],i[1],i[2],i[3])
                d[j] = (dx[0],dx[1],dx[2],dx[3],dx[4])
        else:
            for j in dihedrals:
                print(j)
                i = j.split('-')
                dx = self.db.dihedral(i[0],i[1],i[2],i[3])
                d[j] = (dx[0],dx[1],dx[2],dx[3])
        return b, a, d

    def generate_pairs(self,types):
        self.pairs = {}
        for i in range(len(types)):
            for j in range(i,len(types)):
                a= ( numpy.sqrt(self.db.epsilon(types[i],key='type')*self.db.epsilon(types[j],key='type')), numpy.sqrt(self.db.sigma(types[i],key='type')*self.db.sigma(types[j],key='type')) )
                self.pairs[(types[i],types[j])] = a
                print(a)
        return self.pairs

    def generate_LJ(self,types):
        """Determines LJ values for each interaction set.

            Args:
                types (list): List of atom types is used to determine the maximum sigma an epsilon
                              for normalizing the interactions.
        """
        self.pairs = {}
        self.max_sig = 0.0
        self.max_sig_i = -1
        self.max_eps = 0.0
        self.max_eps_i = -1
        self.max_mass = 0.0
        for i in range(len(types)):
            for j in range(i,len(types)):
                a= ( numpy.sqrt(self.db.epsilon(types[i],key='type')*self.db.epsilon(types[j],key='type')), numpy.sqrt(self.db.sigma(types[i],key='type')*self.db.sigma(types[j],key='type')) )
                self.pairs[(types[i],types[j])] = a
            if self.db.sigma(types[i], key='type') > self.max_sig:
                self.max_sig = self.db.sigma(types[i],key='type')
                self.max_sig_i = types[i]
            if self.db.epsilon(types[i],key='type') >self.max_eps:
                self.max_eps = self.db.epsilon(types[i],key='type')
                self.max_eps_i = types[i]
            if self.db.mass(types[i],key='type') >self.max_mass:
                self.max_mass = self.db.mass(types[i],key='type')
        kb = 1.38e-23 #J/K
        print("Normalizing by sigma=", self.max_sig, "Angstroms.  Type:", self.max_sig_i)
        e = trappe_e_to_J(self.max_eps)
        print("And by epsilon=", e, "Joules.  Type:", self.max_eps_i)
        for key,val in self.pairs.items():
            self.pairs[key] = (val[0]/self.max_eps, val[1]/self.max_sig)
            print(key, self.pairs[key])
        Tr = [0, 20, 100, 200, 300, 400, 500, 1000]
        Td = [0.5, 1., 2., 2.5, 3., 3.5, 4., 5., 6.]
        print("For Reference:")
        for t in Tr:
            print("T = {0} C, {1}K: {2}".format( t, t+273.15, (t+273.15)*kb/e))
        for t in Td:
            print("T = {0}: {1} C".format( t, t*e/kb-273.15 ))
        #self.max_eps=e

    def generate_charges(self,types):
        """For a list of atom types (types), look up charges."""
        self.charges = []
        perm0=8.854e-12 #m-3 kg-1 s4 A2
        elec_c = 1.6e-19 #C
        norm = numpy.sqrt(4*numpy.pi*perm0*self.max_sig*1e-10*self.max_eps*1.38e-23)
        for t in types:
            self.charges.append(self.db.charge(t,key='type')*elec_c/norm)

    def get_bond_length(self,j):
        """Returns normalized bond length for bond j (a string)."""
        i = j.split('-')
        b = self.db.bond(i[0],i[1])
        return b[0]/self.max_sig

    def generate_bonds(self,types):
        """For a list of bond types (types), normalize the bond lengths and bond k's."""
        self.bonds = {}
        for j in types:
            if j !='vis':
                i = j.split('-')
                b = self.db.bond(i[0],i[1])
                self.bonds[j] = (b[0]/self.max_sig, b[1]/self.max_eps) #convert lengths and energies

    def generate_angles(self,types,units="radians"):
        """For a list of angle types (types), normalize the angles and angle  k's."""
        self.angles = {}
        for j in types:
            i = j.split('-')
            b = self.db.angle(i[0],i[1], i[2])
            if units is "degrees":
                self.angles[j] = (numpy.pi*b[0]/180., b[1]/self.max_eps) #just convert energies
            if units is "radians":
                self.angles[j] = (b[0], b[1]/self.max_eps) #just convert energies

    def generate_dihedrals(self,types):
        """For a list of dihedral types (types), normalize the dihedral V's."""
        self.dihedrals = {}
        for j in types:
            print(j)
            i = j.split('-')
            b = self.db.dihedral(i[0],i[1],i[2],i[3])
            self.dihedrals[j] = (b[0],b[1],b[2],b[3])

    def generate_masses(self,types):
        """For a list of atom types (types), normalize masses."""
        self.masses = []
        for t in types:
            self.masses.append(self.db.mass(t,key='type')/self.max_mass)

    def generate_diameters(self,types):
        """For a list of atom types (types), normalize diameters."""
        self.diameters = []
        for t in types:
            self.diameters.append(self.db.sigma(t,key='type')/self.max_sig)
