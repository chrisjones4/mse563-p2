from cme_utils.job_control import manager
import sys
import os

if __name__=="__main__":
    hardware = ''
    tps = 0.
    n = 0
    run_id = int(sys.argv[2])
    crashed = 0 
    home_dir = os.environ['HOME']
    if sys.argv[1] == 'mav':
        db = home_dir+'/sim_db.sqlite3'
    else:
        db = 'sim_db.sqlite3'
    with open("{0:06d}.o".format(run_id)) as f: #TODO: fix hardcoded pattern?
        for l in f.readlines():
            if "DRAM" in l:
                hardware = l[:-1]
            if "Average TPS" in l:
                tps = float(l.split()[-1])
            if "positions" in l:
                n = int(l.split()[1])
            if "Traceback" in l:
                crashed = 1

    d = manager.db_manager(db)
    d.update(run_id,'hardware',hardware)
    d.update(run_id,'tps',tps)
    d.update(run_id,'n',n)
    d.update(run_id,'crashed',crashed)
