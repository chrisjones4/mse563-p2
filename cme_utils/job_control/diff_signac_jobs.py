import sys
import argparse
from cme_utils.job_control import signac_helpers

def main():
    parser = argparse.ArgumentParser(description='Takes in a jobid and array of jobs and show differences in statepoints')
    parser.add_argument("j1",  type=str, help="Signac jobid for first job.")
    parser.add_argument("j2",  type=str, help="Signac jobid for second job.")
    parser.add_argument("-w", "--workspace", type=str,default='.',help="Path to signac workspace.")

    args = parser.parse_args()
    if args.workspace is None:
        print("Please provide the signac workspace path using -w option")
        return
    if args.j1 is None:
        print("Please provide the first job id using -j1 option")
        return
    if args.j2 is None:
        print("Please provide the second job id using -j2 option")
        return
    print('type:',type(args.j1),args.j1)
    signac_helpers.diff_jobs(args.workspace,args.j1,args.j2)
